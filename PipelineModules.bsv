package PipelineModules;

import PipelineTypes::*;
import TinyTypes::*;
import GetPut::*;
import BRAM::*;
import RegFile :: * ;
import FIFO::*;
import FIFOF::*;
import SpecialFIFOs::*;

module mkFetchGPStage#(BRAMServer#(PCT, WordT) im, Bool predict) (GPStage);
Reg#(InstructionT) inst <- mkRegU;
Reg#(DataT) data <- mkRegU;
Reg#(PCT) lastPC <- mkReg(0);
Reg#(Bool) first <- mkReg(True);

Wire#(BRAMRequest#(PCT, int)) fetchWire <- mkWire();
Wire#(InstructionT) instWire <- mkWire;
Wire#(DataT) dataOutWire <- mkWire;
Wire#(DataT) dataInWire <- mkWire;

	rule fetch(dataInWire matches tagged Fetch .f);
		PCT nextPC;
			nextPC = f.pc;
			dataOutWire <= Fetch{pc:f.pc};
		lastPC <= nextPC;
		let r = BRAMRequest{write: False, responseOnWrite: False, address: nextPC, datain: 0};
		im.request.put(r);
		$display("%05t: fetch = ",$time,nextPC);
		first <= False; //Bad, although probably optimises
	endrule

	rule fetch_predict(!first&&predict);
		PCT nextPC;
		nextPC = lastPC +1;
		lastPC <= nextPC;
		let r = BRAMRequest{write: False, responseOnWrite: False, address: nextPC, datain: 0};
		im.request.put(r);
		dataOutWire <= Fetch{pc:nextPC};
		$display("%05t: fetch(p) = ",$time,nextPC);
	endrule
	
	interface Get instOut;
		method ActionValue#(InstructionT) get();
			return instWire;
		endmethod
	endinterface

	interface Put instIn;
		method Action put(InstructionT x);
			instWire <= x;
		endmethod
	endinterface

	interface Get dataOut;
		method ActionValue#(DataT) get();
			return dataOutWire;
		endmethod
	endinterface

	interface Put dataIn;
		method Action put(DataT x);
			dataInWire <= x;
			
		endmethod
	endinterface

endmodule
 

module mkRegFetchGPStage#(BRAMServer#(PCT, WordT) im, RegFile#(RegT, WordT) rf) (GPStage);///Sizeof PCT?
Reg#(InstructionT) inst <- mkRegU;
Reg#(DataT) data <- mkRegU;
Wire#(InstructionT) instWire <- mkWire;
Wire#(DataT) dataInWire <- mkWire();
Wire#(DataT) dataOutWire <- mkWire();
FIFO#(DataT) dataOutFIFO <- mkBypassFIFO();

	rule regFetch;
		let read <- im.response.get();
		let i = word2instruction(read);
		instWire <= i;

		let ra = rf.sub(i.Normal.ra);
		let rb = rf.sub(i.Normal.rb);
		let data = tagged RegVal{valRa:ra, valRb:rb, pc:dataInWire.Fetch.pc};
		dataOutFIFO.enq(data);
		//$display("%05t: regFetch ran ",$time);
		$display("%05t: decode = ",$time,data.RegVal.valRa,data.RegVal.valRb);
	endrule
	
	interface Get instOut;
		method ActionValue#(InstructionT) get();
			return instWire;
		endmethod
	endinterface

	interface Put instIn;
		method Action put(InstructionT x);
		endmethod
	endinterface

	//interface Get dataOut;
	//	method ActionValue#(DataT) get();
	//		return dataOutFIFO.deq();
	//	endmethod
	//endinterface

	interface dataOut = toGet(dataOutFIFO);

	interface Put dataIn;
		method Action put(DataT x);
			dataInWire <= x;
		endmethod
	endinterface

endmodule


module mkALUGPStage#(BRAMServer#(PCT, WordT) dm, FIFOF#(WordT) inQ) (GPStage);
Reg#(InstructionT) inst <- mkRegU;
Reg#(DataT) data <- mkRegU;
Reg#(WordT) alu <- mkReg(0);
Wire#(InstructionT) instWire <- mkWire;
Wire#(DataT) dataOutWire <- mkWire();
Wire#(DataT) dataInWire <- mkWire();

	rule main;
		WordT alu_result;
		Bool doSkip = False; //TODO: bad init
		let valRa = dataInWire.RegVal.valRa;
		let valRb = dataInWire.RegVal.valRb;

		case (instWire) matches //Switch on the current instruction
	 	tagged Normal {func:.func, shift:.shift, op:.op, skip:.skip}: //If tagged normal, place into normal structure and access func, shift, skip and op.
	    begin
	       	WordT alu_calc;
	       // initiate loads
	       	if(op==OpLoadDM) //If load data, place data load request
		  		dm.request.put(BRAMRequest{write: False, responseOnWrite: False, address: unpack(truncate(pack(valRb))), datain: 0}); //data load request for value in Rb
	       // first do the ALU operation
	       	case(func) //Switch on function
		  		FaADDb: alu_calc = valRa+valRb;
				FaSUBb: alu_calc = valRa-valRb;
				FINCb:  alu_calc = valRb+1;
				FDECb:  alu_calc = valRb-1;
				FaANDb: alu_calc = valRa & valRb;
				FaORb:  alu_calc = valRa | valRb;
				FaXORb: alu_calc = valRa ^ valRb;
				default: begin alu_calc = 0; $display("Reserved inst, finish"); $finish; end //If function does not match then kill program(?)
	       	endcase
	       // then do the shifter (rotates)
	       	Bit#(32) alub = pack(alu_calc); // ALU result in bits
	       	case(shift)
			    ShiftNone:  alu_result = unpack(alub);
			    ShiftRCY1:  alu_result = unpack({alub[0],alub[31:1]});
			    ShiftRCY8:  alu_result = unpack({alub[7:0],alub[31:8]});
			    ShiftRCY16: alu_result = unpack({alub[15:0],alub[31:16]});
			    default:    alu_result = unpack(alub);
	       	endcase
	       	doSkip = ((skip==SkipNeg) && (alu_result<0)) ||
	                 ((skip==SkipZero) && (alu_result==0)) ||
 			 ((skip==SkipInRdy) && inQ.notEmpty); //If instruction says skip and condition is met, set doSkip true
	    end
		tagged Immediate {rw:.rw, imm:.imm}: //If tagged immediate, place into Immediate structure and access rw and imm.
	    begin
	       	doSkip = False; //Never skip
	       	alu_result = zeroExtend(unpack(pack(imm))); //Take the immediate value and prepend zeros to make a WordT
	    end
	 	default: alu_result = 0; //If untagged, set to 0
      	endcase

      	alu <= alu_result; //Move result into alu


      	let data = ALU{valRa:valRa, valRb:valRb, alu:alu_result, doSkip:doSkip, pc:dataInWire.RegVal.pc};
		dataOutWire <= data;
		$display("%05t: alu    = ",$time,data.ALU.valRa,data.ALU.valRb,data.ALU.alu,valRa+valRb);
	endrule
	
	interface Get instOut;
		method ActionValue#(InstructionT) get();
			return instWire;
		endmethod
	endinterface

	interface Put instIn;
		method Action put(InstructionT x);
			instWire <= x;
		endmethod
	endinterface

	interface Get dataOut;
		method ActionValue#(DataT) get();
			return dataOutWire;
		endmethod
	endinterface

	interface Put dataIn;
		method Action put(DataT x);
			//data <= x;
			dataInWire <= x;
		endmethod
	endinterface


endmodule

module mkWriteGPStage#(BRAMServer#(PCT, WordT) im, BRAMServer#(PCT, WordT) dm, BRAMServer#(PCT, WordT) dm2, RegFile#(RegT, WordT) rf, FIFOF#(WordT) inQ, FIFO#(WordT) outQ) (GPStage);///Sizeof PCT?
Reg#(InstructionT) inst <- mkRegU;
Reg#(DataT) data <- mkRegU;
Wire#(InstructionT) instWire <- mkWire;
Wire#(DataT) dataInWire <- mkWire();
Wire#(DataT) dataOutWire <- mkWire();
FIFO#(DataT) dataOutFIFO <- mkBypassFIFO();
Reg#(PCT) expectedPC <- mkReg(0);

	rule writeBack(expectedPC == dataInWire.ALU.pc);
		let alu = dataInWire.ALU.alu;
		let valRa = dataInWire.ALU.valRa;
		let valRb = dataInWire.ALU.valRb;
		let doSkip = dataInWire.ALU.doSkip;
		let pc = dataInWire.ALU.pc;
		case (instWire) matches
		tagged Normal {op:.op, rw:.rw}: //If instruction tagged normal, access op and rw
	    begin
	       	pc = doSkip ? pc+2 : ((op==OpJump) ? unpack(truncate(pack(alu))) : pc+1); //pc+1 unless skip then pc+2 or jump then pc=truncated alu.
	       	Maybe#(WordT) wbval=tagged Invalid; //write-back value initially invalid
	       	case (op)// Case on operation
		  	OpNormal, OpStoreDM, OpStoreIM, OpOut: wbval = tagged Valid alu; //if normal, store DM/IM or out then write-back is alu and valid.
		  	OpLoadDM: begin //If loadDM then get the response from the data memory and set valid.
			    let read <- dm.response.get();
		       	wbval = tagged Valid read;
			end
		  	OpIn: begin //If input then get front of input queue and set valid.
			    wbval = tagged Valid inQ.first;
			  	inQ.deq;
			end
			OpJump: begin
				wbval = tagged Valid unpack(zeroExtend(pack(dataInWire.ALU.pc + 1)));
			end
	       	endcase
	       	
	       	if(isValid(wbval)) begin//If valid
		    	rf.upd(rw, fromMaybe(?,wbval)); //If valid then set register at write location to value, else don't care
		    	$display("%05t: r%1d <- %1d",$time,rw,fromMaybe(?,wbval));
		  	end else begin
		  		//$display("%05t: alu = %1d",$time,alu);
		  	end
	       	PCT addr = unpack(truncate(pack(valRb))); //Address is Rb truncated
	       	case (op)
		  	OpStoreDM: dm2.request.put(BRAMRequest{write: True, responseOnWrite: False, address: addr, datain: valRa}); //Write Ra to DM[addr]
		  	OpStoreIM: im.request.put(BRAMRequest{write: True, responseOnWrite: False, address: addr, datain: valRa}); //Write Ra to IM[addr]
		  	OpOut:     outQ.enq(alu); //Enqueue alu onto output queue
		  	OpReserved: $finish; //If reserved, exit
	       	endcase
	    end
	 	tagged Immediate {rw:.rw, imm:.imm}: //If tagged immediate, simple
	    begin
	       	pc = pc+1; //Increment pc
	       	rf.upd(rw, alu); //Write alu to register rw
	       	//$display("%05t: r%1d = imm = %1d",$time,rw,alu);
	    end
      	endcase

		//dataOutWire <= tagged Fetch{pc:pc};
		expectedPC <= pc;
		DataT data = tagged Fetch{pc:pc};
		dataOutFIFO.enq(data);
		$display("%05t: writeback    =  ",$time,pc,data.Fetch.pc,dataInWire.ALU.doSkip);
	endrule

	rule stall(expectedPC != dataInWire.ALU.pc);
		$display("%05t: writeback stalled expecting %1d but got %2d",$time,expectedPC,dataInWire.Fetch.pc);
		case (instWire) matches
		tagged Normal {op:.op, rw:.rw}: //If instruction tagged normal, access op and rw
	    begin
			case (op)
			OpLoadDM: let a <- dm.response.get();
			endcase
		end
		endcase
	endrule
	
	interface Get instOut;
		method ActionValue#(InstructionT) get();
			return instWire;
		endmethod
	endinterface

	interface Put instIn;
		method Action put(InstructionT x);
			instWire <= x;
			inst <= x;
		endmethod
	endinterface

	/*interface Get dataOut;
		method ActionValue#(DataT) get();
			return dataOutWire;
		endmethod
	endinterface*/

	interface dataOut = toGet(dataOutFIFO);

	interface Put dataIn;
		method Action put(DataT x);
			dataInWire <= x;
		endmethod
	endinterface

endmodule

endpackage: PipelineModules