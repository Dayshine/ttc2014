package PipelineTestBench;

import TestSuite::*;
import PipelineTypes::*;
import PipelineModules::*;
import PipelineTiny3::*;
import BRAM::*;
import RegFile::*;
import TinyTypes::*;
import TinyAsm::*;
import Connectable::*;
import FIFO::*;
import FIFOF::*;
import SpecialFIFOs::*;

module mkTestBenchM(Empty);

	BRAM2Port#(PCT, WordT) im <- mkBRAM2Server(defaultValue);
	BRAM2Port#(PCT, WordT) dm <- mkBRAM2Server(defaultValue);

	RegFile#(RegT, WordT) rf <- mkRegFileFull();
	FIFO#(WordT) outQ <- mkFIFO();
	FIFOF#(WordT) inQ <- mkFIFOF();

	GPStage fetch <- mkFetchGPStage(im.portA,False);
	GPStage regFetch <- mkRegFetchGPStage(im.portA, rf);
	GPStage alu <- mkALUGPStage(dm.portA, inQ);
	GPStage writeback <- mkWriteGPStage(im.portB, dm.portA, dm.portB, rf, inQ, outQ);

	FIFO#(InstructionT) instPipe <- mkPipelineFIFO();
	FIFO#(DataT) dataPipe <- mkPipelineFIFO();
	mkConnection(toPut(dataPipe),fetch.dataOut);
	mkConnection(toGet(dataPipe),regFetch.dataIn);
	mkConnection(toPut(instPipe),fetch.instOut);
	mkConnection(toGet(instPipe),regFetch.instIn);

	FIFO#(InstructionT) instPipe2 <- mkPipelineFIFO();
	FIFO#(DataT) dataPipe2 <- mkPipelineFIFO();
	mkConnection(toPut(dataPipe2),regFetch.dataOut);
	mkConnection(toGet(dataPipe2),alu.dataIn);
	mkConnection(toPut(instPipe2),regFetch.instOut);
	mkConnection(toGet(instPipe2),alu.instIn);

	FIFO#(InstructionT) instPipe3 <- mkPipelineFIFO();
	FIFO#(DataT) dataPipe3 <- mkPipelineFIFO();
	mkConnection(toPut(dataPipe3),alu.dataOut);
	mkConnection(toGet(dataPipe3),writeback.dataIn);
	mkConnection(toPut(instPipe3),alu.instOut);
	mkConnection(toGet(instPipe3),writeback.instIn);



	Reg#(UInt#(8)) index <- mkReg(0);

	Reg#(UInt#(3)) phase <- mkReg(0);

	rule init if (phase==0);
		phase <= 1;
		InstructionT inst = tagged Normal{rw: 4, ra:0, rb:1, func:FaADDb, shift:ShiftNone, skip:SkipNever, op:OpNormal};
		InstructionT inst2 = tagged Normal{rw: 5, ra:2, rb:3, func:FaADDb, shift:ShiftNone, skip:SkipNever, op:OpNormal};
		im.portA.request.put(BRAMRequest{write: True, responseOnWrite: False, address: 0, datain: instruction2word(inst)});
		im.portB.request.put(BRAMRequest{write: True, responseOnWrite: False, address: 1, datain: instruction2word(inst2)});
		rf.upd(0,2);
	endrule

	rule fillReg if (phase==1);
		rf.upd(1,4);
		InstructionT inst3 = tagged Normal{rw: 0, ra:4, rb:5, func:FaADDb, shift:ShiftNone, skip:SkipNever, op:OpNormal};
		im.portA.request.put(BRAMRequest{write:True, responseOnWrite: False, address: 2, datain: instruction2word(inst3)});
		phase <=2;
	endrule

	rule fillReg2 if (phase==2);
		rf.upd(2,3);
		phase <=3;
	endrule

	rule fillReg3 if (phase==3);
		rf.upd(3,5);
		phase <=4;
	endrule
	rule movestages if (phase==4);



		if(index==0) begin
		$display("%05t: start",$time);
		DataT d = tagged Fetch {pc:0};
		fetch.dataIn.put(d);
		end else if(index==1) begin
		DataT d = tagged Fetch {pc:1};
		fetch.dataIn.put(d);
		end else if(index < 10) begin
		DataT d = tagged Fetch {pc:2};
		fetch.dataIn.put(d);
		end else if(index==10)
		$finish;

		
		index <= index+1;
	endrule
endmodule

module mkTestBenchP(Empty);
Reg#(UInt#(2)) phase <- mkReg(3);

	function List#(AsmLineT) testThree(function ImmT findaddr(String label)) =
		cons(asmi("",    0, 0), // put 0 in r0
		cons(asmi("",    1, 2), // put 2 in r1
		cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),
		cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 1, 1, 1), 
		cons(asm("",	 OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0),
		cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
		tagged Nil))))));

	function List#(AsmLineT) testThreeb(function ImmT findaddr(String label)) =
		cons(asmi("",    0, 0), // put 0 in r0
		cons(asmi("",    1, 2), // put 1 in r1
		cons(asmi("",    2, 4),
		cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),
		cons(asm("",     OpNormal, FaORb, ShiftNone, SkipNever, 1, 1, 1),
		cons(asm("",     OpNormal, FDECb, ShiftNone, SkipNever, 2, 2, 2),
		cons(asm("",	 OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0),
	    cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 1, 1, 1), 
		cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 2, 2, 2),
		cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
		tagged Nil))))))))));

   	InstructionROM_T irom = assembler(testThreeb);
   
   	TinyCompIfc tiny <- mkPipeTinyComp(irom);

   	rule a if (phase == 0);
   		let val = -402653184;
   		tiny.in.put(val);
   		phase <= phase +1;
   	endrule

   	rule b if (phase == 1);
   		let val = -134217728;
   		tiny.in.put(val);
   		phase <= phase +1;
   	endrule

   	rule c if (phase == 2);
   		let val = 20;
   		tiny.in.put(val);
   		phase <= phase +1;
   	endrule

   	rule handle_output;
    	let out <- tiny.out.get();
      	$display("%05t: output = %d\n",$time,out);
   	endrule
	
endmodule

module mkTestBenchMand(Empty);
	InstructionROM_T irom = assembler(mandel);
   
   	TinyCompIfc tiny <- mkPipeTinyComp(irom);

   	let fh <- mkReg(InvalidFile);
   	Reg#(Int#(5)) phase<-mkReg(0);
   	Reg#(Int#(8)) count<-mkReg(1);
   	Reg#(Int#(8)) i <- mkReg(0);
   	Reg#(Int#(8)) j <- mkReg(0);
   	let width = 16;
   	let height = 12;
   	let depth = 15;
   	rule open_file(phase==0);
   		String bmpFile = "output.bmp";
   		File lfh <- $fopen(bmpFile, "w");
   		if (lfh == InvalidFile)
   			begin
   			$display("cannot open %s", bmpFile);
   			$finish(0);
   		end
   		fh<=lfh;
   		//String header = ;
   		//UInt#(8) body = 128;
   		//Bit#(4) front = unpack(pack(body)[7:4]);
   		//Bit#(4) back = unpack(pack(body)[3:0]);
   		//$fwriteh(fh,front,back,front,back,front,back,front,back);
   		//$fwriteh(fh,front,back,front,back,front,back,front,back);
   		//$fwriteh(fh,front,back,front,back,front,back,front,back);
   		//$fwriteh(fh,front,back,front,back,front,back,front,back);

   		phase<=phase+1;
   	endrule

   	rule x(phase==1);

   		/*let x= extend(count-1);
   		x = x * 3145728;
   		x = x / width;
   		x = x * 256;
   		x = x + -536870912;*/
   		WordT x = ((-2) << 28) + (((3 << 20) * extend(i) / width) << 8);



   		tiny.in.put(x);

   		phase<=phase+1;
   	endrule

   	rule y(phase==2);
   	   	/*let y = extend(count-1);
   	   	y = y * 2097152;
   	   	y = y / height;
   	   	y = y * 256;
   	   	y = y + -268435456;*/

   		WordT y = ((1) << 28) - (((2 << 20) * extend(j) / height) << 8);
   		tiny.in.put(y);
   	   	phase<=phase+1;
   	endrule

   	rule iter(phase==3);
   	   	tiny.in.put(depth);

   	   	if(i<(width-1))
   			i <= i + 1;
   		else begin
   			i <= 0;
   			j <= j + 1;
   		end

   		if(count == ((width*height)+1))
   			$finish;
   		else begin
   			count <= count + 1;
   			phase <= 1;
   		end
   	endrule

   	rule handle_output;
    	let out <- tiny.out.get();
    	//out = (out-1) * 16;
    	if(out == depth)
    		out = 255;
    	else
    		out = 0;
    	Bit#(4) alpha = 'b1111;
    	Bit#(4) front = unpack(pack(out)[7:4]);
   		Bit#(4) back = unpack(pack(out)[3:0]);
    	$fwriteh(fh,front,back,front,back,front,back,alpha,alpha);
    	$fflush(fh);
      	$display("%05t: output = %d\n",$time,out);
   	endrule

endmodule

endpackage: PipelineTestBench