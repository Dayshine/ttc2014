package PipelineTiny3;

import PipelineModules::*;
import PipelineTypes::*;
import TinyTypes::*;
import TinyAsm::*;
import GetPut::*;
import BRAM::*;
import FIFO::*;
import FIFOF::*;
import SpecialFIFOs::*;
import RegFile::*;
import Connectable::*;

interface TinyCompIfc;
	interface Put#(WordT) in;
	interface Get#(WordT) out;
endinterface

typedef enum {INIT, START, RUNNING} PhaseT deriving (Eq, Bits);

module mkPipeTinyComp(InstructionROM_T irom, TinyCompIfc ifc);
//init vars
Reg#(PhaseT) phase <- mkReg(INIT);
Reg#(PCT) addr <- mkReg(0);

//external mem
BRAM2Port#(PCT, WordT) im <- mkBRAM2Server(defaultValue);
BRAM2Port#(PCT, WordT) dm <- mkBRAM2Server(defaultValue);

//external access
FIFOF#(WordT) inQ <- mkFIFOF; //Input FIFO Int#(32) of Words
FIFO#(WordT) outQ <- mkFIFO; //Output FIFO Int#(32) of Words

RegFile#(RegT, WordT) rf <- mkRegFileFull();
FIFO#(DataT) pc <- mkPipelineFIFO();

GPStage fetch <- mkFetchGPStage(im.portA,False);
GPStage regFetch <- mkRegFetchGPStage(im.portA, rf);
GPStage alu <- mkALUGPStage(dm.portA, inQ);
GPStage writeback <- mkWriteGPStage(im.portB, dm.portA, dm.portB, rf, inQ, outQ);

mkConnection(toGet(pc),fetch.dataIn);

FIFO#(InstructionT) instPipe <- mkPipelineFIFO();
FIFO#(DataT) dataPipe <- mkPipelineFIFO();
mkConnection(toPut(dataPipe),fetch.dataOut);
mkConnection(toGet(dataPipe),regFetch.dataIn);
mkConnection(toPut(instPipe),fetch.instOut);
mkConnection(toGet(instPipe),regFetch.instIn);

FIFO#(InstructionT) instPipe2 <- mkPipelineFIFO();
FIFO#(DataT) dataPipe2 <- mkPipelineFIFO();
mkConnection(toPut(dataPipe2),regFetch.dataOut);
mkConnection(toGet(dataPipe2),alu.dataIn);
mkConnection(toPut(instPipe2),regFetch.instOut);
mkConnection(toGet(instPipe2),alu.instIn);

FIFO#(InstructionT) instPipe3 <- mkPipelineFIFO();
FIFO#(DataT) dataPipe3 <- mkPipelineFIFO();
mkConnection(toPut(dataPipe3),alu.dataOut);
mkConnection(toGet(dataPipe3),writeback.dataIn);
mkConnection(toPut(instPipe3),alu.instOut);
mkConnection(toGet(instPipe3),writeback.instIn);

//FIFO#(DataT) dataPipe4 <- mkPipelineFIFO();
//mkConnection(toPut(dataPipe4),writeback.dataOut);
//mkConnection(toGet(dataPipe4),fetch.dataIn);

	rule do_init(phase==INIT);
    	WordT data = unpack(pack(irom[addr])); //Take the next instruction from the irom and convert to wordT.
      	im.portA.request.put(BRAMRequest{write: True, responseOnWrite: False, //Write instruction to instruction memory.
					address: addr, datain: data});
      
      	InstructionT inst = unpack(pack(data)); //Take WordT instruction and convert to InstructionT
      	if(inst.Normal.op!=OpReserved) //if not reserved instruction, print instruction
	 		$display("%05t: init pm[%1d] = ",$time,addr,disassemble(inst));
      	let next_addr = addr+1; //Move to next instruction
      	addr <= next_addr;
      	if(next_addr==0) //If next addr is 0 (i.e rolled over) then move to next phase
	 		phase <= START;
    endrule

    rule start(phase==START);
    	DataT d = tagged Fetch {pc:0};
		pc.enq(d);
		phase <= RUNNING;
    endrule

    rule pcUpdate(phase == RUNNING);
    	DataT pcData <- writeback.dataOut.get();
    	if (pcData matches tagged Fetch .d) begin
    		pc.enq(pcData);
    	end else
    		pc.enq(tagged Fetch {pc:0});
    	//$display("TEST");
    	//$display("TEST",typ)
    	//pc.enq(pcData);
    	//if (pcData matches tagged Fetch)
    	//	pc.enq(pcData);
    	//else
    	//	pc.enq(tagged Fetch {pc:0});
    endrule


    interface in = toPut(inQ);
    interface out = toGet(outQ);


endmodule

endpackage: PipelineTiny3