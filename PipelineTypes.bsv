package PipelineTypes;

import GetPut::*;
import TinyTypes::*;

interface GPStage;
	interface Get#(InstructionT) instOut;
	interface Put#(InstructionT) instIn;
	interface Get#(DataT) dataOut;
	interface Put#(DataT) dataIn;
endinterface

typedef union tagged {
   struct {
      WordT   valRa;
      WordT   valRb;
      PCT pc;
   } RegVal;
   struct {
      WordT   valRa;
	   WordT   valRb;
	   WordT   alu;
	   Bool    doSkip;
      PCT pc;
      } ALU;
   struct {
     PCT pc;
   } Fetch;
} DataT deriving (Bits, Eq);


endpackage: PipelineTypes