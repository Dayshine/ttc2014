package TestSuite;

import TinyTypes::*;
import TinyAsm::*;

//Data hazard test
//Outputs 10
function List#(AsmLineT) testOne(function ImmT findaddr(String label)) =
	cons(asmi("",    0, 0), // put 0 in r0
 	cons(asmi("",    1, 1), // put 1 in r1
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 0, 1),
  	cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 1, 0, 1), 
  	cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
  	tagged Nil)))))))))))));
//Skip test
//Outputs 0
function List#(AsmLineT) testOneb(function ImmT findaddr(String label)) =
	cons(asmi("",    0, 0), // put 0 in r0
	cons(asm("",     OpNormal, FaORb, ShiftNone, SkipZero, 0, 0, 0), // Do nothing, skip next
	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0), // INC r0 to 1, should be skipped
	cons(asm("",	   OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0), //Should be 0
	cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
	tagged Nil)))));

//Jump test
//Outputs 20
function List#(AsmLineT) testOnec(function ImmT findaddr(String label)) =
	cons(asmi("",	   0, 0),
	cons(asmi("",	   1, 10),
	cons(asmi("",	   2, findaddr("loop")),
	cons(asm("loop", OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 1, 0), //1 for debugging
	cons(asm("",	   OpNormal, FDECb, ShiftNone, SkipZero,  1, 0, 1),
	cons(asm("",     OpJump,   FaORb, ShiftNone, SkipNever, 3, 2, 2),
	cons(asm("",     OpOut,    FaORb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("",    OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
	tagged Nil)))))))));

//Short data hazard test
//Total execution time should be 13+6+4=23 clocks.
function List#(AsmLineT) testOned(function ImmT findaddr(String label)) =
	cons(asmi("",    0, 0), // put 0 in r0
 	cons(asmi("",    1, 1), // put 1 in r1
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),//Stall here once
	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 1, 1),//Should continue as r1 has already cleared
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),//Again stall
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 1, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),//again
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 1, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),//again
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 1, 1, 1),
  	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),//again
  	cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 1, 1, 1), 
  	cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0), //again, total 6
  	cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
  	tagged Nil))))))))))))));

//Function to test writing and immediately reading instructions in memory.
//0000000|00000000000000|010|00|00|000 Increment r0.
//Outputs 1
function List#(AsmLineT) testTwo(function ImmT findaddr(String label)) =
	cons(asmi("",	0,	0),//r0=0
	cons(asmi("",	1,	2),//r1=2
	cons(asmi("",	2,	5),//r2=5
	cons(asmi("",	4,	findaddr("overwrite")),
	cons(asm("",	OpNormal, FINCb, ShiftRCY8, SkipNever, 3, 0, 0),//r3=0+1RCY8
	cons(asm("",	OpStoreIM,FaORb, ShiftNone, SkipNever, 0, 3, 4),//Store r3 into overwrite location
    cons(asmi("",   0,  0),
	cons(asm("overwrite", OpNormal, FaORb, ShiftNone, SkipNever,0, 0,	0), //Leave r0 as it is, should be overwritten
	cons(asm("",	OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0), //Should be 1 if overwrite succeeded.
	cons(asm("",    OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
	tagged Nil))))))))));

//Flat, no shared registers adjacent
//Outputs 2 then 1
function List#(AsmLineT) testThree(function ImmT findaddr(String label)) =
	cons(asmi("",    0, 0), // put 0 in r0
	cons(asmi("",    1, 2), // put 2 in r1
	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 1, 1, 1), 
	cons(asm("",	 OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
	tagged Nil))))));

//Flat, no shared registers adjacent
//Outputs 1,2,3
function List#(AsmLineT) testThreeb(function ImmT findaddr(String label)) =
	cons(asmi("",    0, 0), // put 0 in r0
	cons(asmi("",    1, 2), // put 1 in r1
	cons(asmi("",    2, 4),
	cons(asm("",     OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("",     OpNormal, FaORb, ShiftNone, SkipNever, 1, 1, 1),
	cons(asm("",     OpNormal, FDECb, ShiftNone, SkipNever, 2, 2, 2),
	cons(asm("",	 OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0),
    cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 1, 1, 1), 
	cons(asm("",     OpOut, FaORb, ShiftNone, SkipNever, 2, 2, 2),
	cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
	tagged Nil))))))))));

//Outputs 0, 1, 2
//More complicated Jump test
 function List#(AsmLineT) testFour(function ImmT findaddr(String label)) =     	
	cons(asmi("",	0, 0),
	cons(asmi("",   1, findaddr("one")),
	cons(asmi("",   2, findaddr("two")),
	cons(asmi("",   3, findaddr("three")),
	cons(asmi("",   4, findaddr("end")),
	cons(asm("",    OpJump, FaORb, ShiftNone, SkipNever, 5, 1, 1),
	cons(asm("three",OpNormal, FINCb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("", OpJump, FaORb, ShiftNone, SkipNever, 5, 4, 4),
	cons(asm("two", OpOut, FINCb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("", OpJump, FaORb, ShiftNone, SkipNever, 5, 3, 3),
	cons(asm("one", OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("", OpJump, FaORb, ShiftNone, SkipNever, 5, 2, 2),
	cons(asm("end", OpOut, FaORb, ShiftNone, SkipNever, 0, 0, 0),
	cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
	tagged Nil))))))))))))));

//Test RW on jump
 function List#(AsmLineT) jumpTest(function ImmT findaddr(String label)) =     	
	cons(asmi("",	0, 0),
	cons(asmi("",   1, findaddr("one")),
	cons(asm("",    OpJump, FaORb, ShiftNone, SkipNever, 5, 1, 1),
	cons(asm("",    OpJump, FaORb, ShiftNone, SkipNever, 5, 1, 1),
	cons(asm("one", OpOut,  FaORb, ShiftNone, SkipNever, 5, 5, 5),
    cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
	tagged Nil))))));

   function List#(AsmLineT) mul16(function ImmT findaddr(String label)) =
   	cons(asmi("",	0,		 0),
   	cons(asmi("",	1,		 1),
   	cons(asmi("",   6,		 findaddr("loop")),
    cons(asmi("",   7,		 findaddr("end")),
    cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever,	2,	0,	0),
    cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever,	3,	0,	0),
    cons(asmi("",	4,		 65535),
    cons(asm("loop",OpNormal,FaANDb,ShiftNone,	SkipNever,	2,	2,	2),
    cons(asm("",    OpNormal,FaANDb,ShiftNone,	SkipZero,	3,	3,	4), //If y==0, exit by jump 1, bitmask to cut the rotated bit
    cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,	5,  5,  5), //Else, skip jump
    cons(asm("",    OpJump,  FaORb,	ShiftNone,	SkipNever,	5,	7,	7), //Jump 1, returns
    cons(asm("",	OpNormal,FaANDb,ShiftNone,  SkipZero,	5,  3,  1), //If y & 1 ==0, skip
    cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,	0,  0,  2), //If y & 1 then += x
    cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever,	3,  3,  3), //Rotate y right one
    cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,	2,  2,  2), //Shift x left (bitwise = *2)
    cons(asm("",	OpJump,  FaORb,	ShiftNone,	SkipNever,	5,  6,  6),
    cons(asm("end", OpOut,	 FaORb, ShiftNone,  SkipNever,  0,  0,  0),
    cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
    tagged Nil))))))))))))))))));

   function List#(AsmLineT) booth16(function ImmT findaddr(String label)) = //C = 2, X=3, Y=4, S=5, U=6, V=7,T1=8, T2=9
   	cons(asmi("",	0,		 0), //Return
   	cons(asmi("",	1,		 32),//Iterator, 16 is size of input
   	cons(asmi("",	2,		 0), //Carry
   	cons(asmi("",	5,		 0), //S = 0
   	cons(asmi("",	6,		 0), //U = 0
   	cons(asmi("",	7,		 0), //V = 0
   	cons(asmi("",	41,		 1), //Constant 1, LSB mask
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever,	42, 41, 41), // 1 RCY 1 is MSB mask
   	cons(asmi("",	44,		 0),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	44, 44, 44),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	44, 44, 44), // -2 is inverse LSB mask  		
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever,	43, 44, 44), // -2 RCY 1 is inverse MSB mask
   	cons(asmi("",	20,		 findaddr("loc1")),
   	cons(asmi("",	21,		 findaddr("loc2")),
   	cons(asmi("",	22,		 findaddr("loc3")),
   	cons(asmi("",	24,		 findaddr("loc5")),
   	cons(asmi("",	25,		 findaddr("loc6")),
   	cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever, 	3,	0,	0), //x
   	cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever,	4,	0,	0), //y
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipNever,	5,	5,	4),//S = -Y
   	cons(asm("loc1",OpNormal,FaORb, ShiftNone,	SkipZero,	2,	2,	2), //Start of loop, if C=0, skip
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	31,	21,	21),//C=1, jump to loc2
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipZero,	31, 3,  41), //If X&1 =0, skip
   	cons(asm("",	OpJump,  FaORb, ShiftNone,  SkipNever,  31, 22, 22), //JMP loc3
   	cons(asm("",	OpJump,	 FaORb, ShiftNone,	SkipNever,	31,	24, 24), //JMP loc6
   	cons(asm("loc3",OpNormal,FaADDb,ShiftNone,	SkipNever,  6,  6,  5), //U = U + S
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,  31, 24, 24),//JMP loc5
   	cons(asm("loc2",OpNormal,FaANDb,ShiftNone,	SkipZero,	31, 3,  41),
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,  31, 24, 24),//JMP loc5
   	cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,  6,  6,  4),
   	cons(asm("",	OpJump,  FaORb,	ShiftNone,	SkipNever,	31, 24, 24),//JMP loc5
   	cons(asm("loc5",OpNormal,FaANDb,ShiftNone,	SkipNever,  8,  6,  42), //U & MSB
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,  9,  6,  41), //U & LSB
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  6,  6,  6),
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  7,  7,  7),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	8,  8,  8),
   	cons(asm("",	OpNormal,FaORb,ShiftNone,	SkipNever,	6,  6,  42), //U = U & ~MSB
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	8,  8,  8),
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,	31, 31, 31),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	6,  6,  43),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	9,  9,  9),
   	cons(asm("",	OpNormal,FaORb,ShiftNone,	SkipNever,	7,  7,  42), 
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	9,  9,  9),
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,	31, 31, 31),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	7,  7,  43),   		
   	cons(asm("loc6",OpNormal,FaANDb,ShiftNone,	SkipNever,  2,  3,  41),
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  3,  3,  3),
   	cons(asm("",	OpNormal,FDECb, ShiftNone,	SkipZero,	1,  1,  1),
   	cons(asm("",	OpJump,	 FaORb, ShiftNone,	SkipNever,  31, 20, 20),
   	cons(asm("",	OpOut,	 FaORb,	ShiftNone,	SkipNever,	6,  6,  6),
   	cons(asm("",	OpOut,   FaORb,	ShiftNone,	SkipNever,  7,  7,  7),
   	cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
    tagged Nil))))))))))))))))))))))))))))))))))))))))))))))))))));

   function List#(AsmLineT) fpmul(function ImmT findaddr(String label)) = //C = 2, X=3, Y=4, S=5, U=6, V=7,T1=8, T2=9
   	cons(asmi("",	0,		 0), //Return
   	cons(asmi("",	1,		 32),//Iterator, 32 is size of input
   	cons(asmi("",	2,		 0), //Carry
   	cons(asmi("",	5,		 0), //S = 0
   	cons(asmi("",	6,		 0), //U = 0
   	cons(asmi("",	7,		 0), //V = 0
   	cons(asmi("",	41,		 1), //Constant 1, LSB mask
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever,	42, 41, 41), // 1 RCY 1 is MSB mask
   	cons(asmi("",	44,		 0),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	44, 44, 44),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	44, 44, 44), // -2 is inverse LSB mask  		
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever,	43, 44, 44), // -2 RCY 1 is inverse MSB mask
   	cons(asmi("",	20,		 findaddr("loc1")),
   	cons(asmi("",	21,		 findaddr("loc2")),
   	cons(asmi("",	22,		 findaddr("loc3")),
   	cons(asmi("",	24,		 findaddr("loc5")),
   	cons(asmi("",	25,		 findaddr("loc6")),
   	cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever, 	3,	0,	0), //x
   	cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever,	4,	0,	0), //y
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipNever,	5,	5,	4),//S = -Y
   	cons(asm("loc1",OpNormal,FaORb, ShiftNone,	SkipZero,	2,	2,	2), //Start of loop, if C=0, skip
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	31,	21,	21),//C=1, jump to loc2
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipZero,	31, 3,  41), //If X&1 =0, skip
   	cons(asm("",	OpJump,  FaORb, ShiftNone,  SkipNever,  31, 22, 22), //JMP loc3
   	cons(asm("",	OpJump,	 FaORb, ShiftNone,	SkipNever,	31,	24, 24), //JMP loc6
   	cons(asm("loc3",OpNormal,FaADDb,ShiftNone,	SkipNever,  6,  6,  5), //U = U + S
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,  31, 24, 24),//JMP loc5
   	cons(asm("loc2",OpNormal,FaANDb,ShiftNone,	SkipZero,	31, 3,  41),
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,  31, 24, 24),//JMP loc5
   	cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,  6,  6,  4),
   	cons(asm("",	OpJump,  FaORb,	ShiftNone,	SkipNever,	31, 24, 24),//JMP loc5
   	cons(asm("loc5",OpNormal,FaANDb,ShiftNone,	SkipNever,  8,  6,  42), //U & MSB
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,  9,  6,  41), //U & LSB
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  6,  6,  6),
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  7,  7,  7),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	8,  8,  8),
   	cons(asm("",	OpNormal,FaORb,ShiftNone,	SkipNever,	6,  6,  42), //U = U & ~MSB
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	8,  8,  8),
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,	31, 31, 31),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	6,  6,  43),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	9,  9,  9),
   	cons(asm("",	OpNormal,FaORb,ShiftNone,	SkipNever,	7,  7,  42), 
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	9,  9,  9),
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,	31, 31, 31),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	7,  7,  43),   		
   	cons(asm("loc6",OpNormal,FaANDb,ShiftNone,	SkipNever,  2,  3,  41),
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  3,  3,  3),
   	cons(asm("",	OpNormal,FDECb, ShiftNone,	SkipZero,	1,  1,  1),
   	cons(asm("",	OpJump,	 FaORb, ShiftNone,	SkipNever,  31, 20, 20),
   	cons(asmi("",	45,		 15), //4 LSBs
   	cons(asmi("",	47,		 0),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	47, 47, 47),
   	cons(asm("",	OpNormal,FaXORb,ShiftNone,  SkipNever,  46, 45, 47), //All but 4 LSB
   	cons(asm("",	OpNormal,FaORb, ShiftRCY16,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY8,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY16,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY8,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	6,	6,	46),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	7,  7,  45),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever,	0,  6,  7),
   	cons(asm("",	OpOut,	 FaORb,	ShiftNone,	SkipNever,	0,  0,  0),
   	cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
    tagged Nil))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

   function List#(AsmLineT) mandel(function ImmT findaddr(String label)) = //C = 2, X=3, Y=4, S=5, U=6, V=7,T1=8, T2=9
   	cons(asmi("",	120,		 findaddr("fpmul")),
   	cons(asmi("",	121,		 findaddr("loop")),
   	cons(asmi("",	122,		 findaddr("finish")),
   	cons(asmi("",	123,		 findaddr("start")),
    cons(asm("start",OpIn,	 FaORb,	ShiftNone,	SkipNever, 	101,0,	0), //x0
   	cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever, 	102,0,	0), //y0
   	cons(asm("",	OpIn,	 FaORb,	ShiftNone,	SkipNever, 	103,0,	0), //maxiter
   	cons(asmi("",	99,      1),
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever, 	99,	99, 99), //x
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever, 	99,	99, 99), //x
   	cons(asmi("",	104,	 0), //x
   	cons(asmi("",	105,	 0), //y
   	cons(asmi("",	106,	 0), //iter
   	cons(asm("loop",OpNormal,FaORb,	ShiftNone,	SkipNever, 	3,	104,104), //x
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever, 	4,	104,104), //x
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	100,120,120),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever,	107,  0,  0), //x^2
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever, 	3,	105,105), //y
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever, 	4,	105,105), //y
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	100,120,120),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever,	108,  0,  0), //y^2
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever, 	3,	104,104), //x
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever, 	4,	105,105), //y
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	100,120,120),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever,	109,  0,  0), //xy
   	cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,  110,109,109), //2xy
   	cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,  111,107,108), //mag^2
  	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipNeg,    31, 99, 111), //4-mag^2
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,   31, 31,  31), //SKIP
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	100,122,122),
  	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,   31, 103,106), //maxiter-iter
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,   31, 31,  31), //SKIP
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	100,122,122),
    cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipNever,	104,107,108), //x=x^2-y^2
   	cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,	104,104,101), //x=x+x0
   	cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,	105,110,102), //y=2xy+y0
   	cons(asm("",	OpNormal,FINCb,	ShiftNone,	SkipNever,	106,106,106), //iter++
   	cons(asm("",	OpJump,  FaORb,	ShiftNone,	SkipNever,	31, 121,121), //restart loop
   	cons(asm("finish",OpOut, FaORb,ShiftNone,	SkipNever,	106,106,106), //output iterations
   	cons(asm("",	OpJump,  FaORb,	ShiftNone,  SkipNever,	31, 123,123),
   	cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
   	//Start fpmul, 3 and 4 are input, 0 is return
   	cons(asmi("fpmul",	0,		 0), //Return
   	cons(asmi("",	1,		 32),//Iterator, 32 is size of input
   	cons(asmi("",	2,		 0), //Carry
   	cons(asmi("",	5,		 0), //S = 0
   	cons(asmi("",	6,		 0), //U = 0
   	cons(asmi("",	7,		 0), //V = 0
   	cons(asmi("",	41,		 1), //Constant 1, LSB mask
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever,	42, 41, 41), // 1 RCY 1 is MSB mask
   	cons(asmi("",	44,		 0),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	44, 44, 44),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	44, 44, 44), // -2 is inverse LSB mask  		
   	cons(asm("",	OpNormal,FaORb,	ShiftRCY1,	SkipNever,	43, 44, 44), // -2 RCY 1 is inverse MSB mask
   	cons(asmi("",	20,		 findaddr("loc1")),
   	cons(asmi("",	21,		 findaddr("loc2")),
   	cons(asmi("",	22,		 findaddr("loc3")),
   	cons(asmi("",	24,		 findaddr("loc5")),
   	cons(asmi("",	25,		 findaddr("loc6")),
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipNever,	5,	5,	4),//S = -Y
   	cons(asm("loc1",OpNormal,FaORb, ShiftNone,	SkipZero,	2,	2,	2), //Start of loop, if C=0, skip
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,	31,	21,	21),//C=1, jump to loc2
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipZero,	31, 3,  41), //If X&1 =0, skip
   	cons(asm("",	OpJump,  FaORb, ShiftNone,  SkipNever,  31, 22, 22), //JMP loc3
   	cons(asm("",	OpJump,	 FaORb, ShiftNone,	SkipNever,	31,	24, 24), //JMP loc6
   	cons(asm("loc3",OpNormal,FaADDb,ShiftNone,	SkipNever,  6,  6,  5), //U = U + S
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,  31, 24, 24),//JMP loc5
   	cons(asm("loc2",OpNormal,FaANDb,ShiftNone,	SkipZero,	31, 3,  41),
   	cons(asm("",	OpJump,	 FaORb,	ShiftNone,	SkipNever,  31, 24, 24),//JMP loc5
   	cons(asm("",	OpNormal,FaADDb,ShiftNone,	SkipNever,  6,  6,  4),
   	cons(asm("",	OpJump,  FaORb,	ShiftNone,	SkipNever,	31, 24, 24),//JMP loc5
   	cons(asm("loc5",OpNormal,FaANDb,ShiftNone,	SkipNever,  8,  6,  42), //U & MSB
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,  9,  6,  41), //U & LSB
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  6,  6,  6),
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  7,  7,  7),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	8,  8,  8),
   	cons(asm("",	OpNormal,FaORb,ShiftNone,	SkipNever,	6,  6,  42), //U = U & ~MSB
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	8,  8,  8),
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,	31, 31, 31),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	6,  6,  43),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	9,  9,  9),
   	cons(asm("",	OpNormal,FaORb,ShiftNone,	SkipNever,	7,  7,  42), 
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipZero,	9,  9,  9),
   	cons(asm("",	OpNormal,FaSUBb,ShiftNone,	SkipZero,	31, 31, 31),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	7,  7,  43),   		
   	cons(asm("loc6",OpNormal,FaANDb,ShiftNone,	SkipNever,  2,  3,  41),
   	cons(asm("",	OpNormal,FaANDb,ShiftRCY1,	SkipNever,  3,  3,  3),
   	cons(asm("",	OpNormal,FDECb, ShiftNone,	SkipZero,	1,  1,  1),
   	cons(asm("",	OpJump,	 FaORb, ShiftNone,	SkipNever,  31, 20, 20),
   	cons(asmi("",	45,		 15), //4 LSBs
   	cons(asmi("",	47,		 0),
   	cons(asm("",	OpNormal,FDECb,	ShiftNone,	SkipNever,	47, 47, 47),
   	cons(asm("",	OpNormal,FaXORb,ShiftNone,  SkipNever,  46, 45, 47), //All but 4 LSB
   	cons(asm("",	OpNormal,FaORb, ShiftRCY16,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY8,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	6,	6,	6),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY16,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY8,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaORb, ShiftRCY1,	SkipNever,	7,	7,	7),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	6,	6,	46),
   	cons(asm("",	OpNormal,FaANDb,ShiftNone,	SkipNever,	7,  7,  45),
   	cons(asm("",	OpNormal,FaORb,	ShiftNone,	SkipNever,	0,  6,  7),
   	cons(asm("",	OpJump	,FaORb, ShiftNone,  SkipNever,  31, 100,100),
   	//End fpmul
   	cons(asm("",	OpOut,	 FaORb,	ShiftNone,	SkipNever,	0,  0,  0),
   	cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
    tagged Nil))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

endpackage: TestSuite