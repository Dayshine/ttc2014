package Tiny3;

import FIFO::*;
import FIFOF::*;
import GetPut::*;
import TinyTypes::*;
import TinyAsm::*;
import BRAM::*;
import Vector::*;

interface TinyCompIfc;
interface Put#(WordT) in;
interface Get#(WordT) out;
endinterface 


module mkTinyComp(InstructionROM_T irom, TinyCompIfc ifc);
  Reg#(PCT) pc <- mkReg(0);
  Reg#(WordT) alu <- mkReg(0);
  Reg#(UInt#(3)) phase <- mkReg(7);
  Reg#(Bool) doSkip <- mkRegU;
  FIFOF#(WordT) inQ <- mkFIFOF;
  FIFO#(WordT) outQ <- mkFIFO;
  Vector#(TExp#(SizeOf#(PCT)),Reg#(WordT)) rf <- replicateM(mkRegU);
  Reg#(WordT) valRa <- mkRegU;
  Reg#(WordT) valRb <- mkRegU;
  Reg#(Vector#(4,InstructionT)) currentInst <- mkReg(newVector);
  Reg#(Vector#(4, PCT)) currentPC <- mkReg(newVector);

  BRAM2Port#(PCT, WordT) im <- mkBRAM2Server(defaultValue);
  BRAM2Port#(PCT, WordT) dm <- mkBRAM2Server(defaultValue);
  Reg#(PCT) addr <- mkReg(0);

  Reg#(Bool) flush <- mkReg(True); //Reset(2) are markers for handling a flush
  Reg#(Bool) flush2 <- mkReg(False);
  Reg#(Bool) flush3 <- mkReg(False);
  Reg#(Bool) jumping <- mkReg(False); //True if the finished inst is a jump
  Reg#(Bool) first <- mkReg(True); //First clock, just stops any queues being popped early.

  Reg#(RegT) toWrite <- mkRegU;
  Reg#(RegT) toWrite2 <- mkRegU;
  Reg#(Bool) toWriteValid <- mkReg(False);
  Reg#(Bool) toWriteValid2 <- mkReg(False);
  Reg#(Bool) stall <- mkReg(False);
  Reg#(Bool) stall2 <- mkReg(False);

  Reg#(PCT) pcNext <- mkReg(0);

  Reg#(Bool) valid[4];
  valid[0] <- mkReg(True);
  valid[1] <- mkReg(False);
  valid[2] <- mkReg(False);
  valid[3] <- mkReg(False);

  

  Reg#(WordT)valRa_WB <- mkRegU;
  Reg#(WordT)valRb_WB <- mkRegU;
  Reg#(WordT)valRa_WB2 <- mkRegU;
  Reg#(WordT)valRb_WB2 <- mkRegU;

  Reg#(WordT) mWriteData <- mkRegU;
  Reg#(PCT) mWriteAddr <- mkRegU;
  Reg#(Bool) imWrite <- mkReg(False);
  Reg#(Bool) dmWrite <- mkReg(False);

  Reg#(UInt#(8)) clockCount <- mkReg(1);

  function Bool stageValid(UInt#(3) curStage);
    return valid[curStage];
  endfunction

  function Vector#(4, element_type) stageStep(Vector#(4, element_type) vect, element_type newFirst);
    Vector#(4, element_type) out = vect;
    if(stall&&!(doSkip||jumping))
      begin
      out[3] = vect[2];
      end
    else
      begin
      out[3] = vect[2];
      out[2] = vect[1];
      out[1] = vect[0];
      out[0] = newFirst;
      end
    return out;
  endfunction

  // initialise instruction memory (im) contents from ROM
  rule do_init(phase==7);
    WordT data = unpack(pack(irom[addr]));
    im.portA.request.put(BRAMRequest{write: True, responseOnWrite: False,
    address: addr, datain: data});

    InstructionT inst = unpack(pack(data));
    if(inst.Normal.op!=OpReserved)
      $display("%05t: init pm[%1d] = ",$time,addr,disassemble(inst));
    let next_addr = addr+1;
    addr <= next_addr;
    if(next_addr==0)
      phase <= 0;

  endrule

  rule main(phase==0);
    let jumpingN = False;
    let doSkipN = False;

    if(stageValid(0))
      begin
      im.portA.request.put(BRAMRequest{write: False, responseOnWrite: False, address: pcNext, datain: 0});
      $write("%05t: fetch",$time," ",pcNext," ");
      end
    else
      $write("%05t: stall",$time," ",pcNext," ");


    if(stall||stall2)
      begin
        $display("| pm[%2d] : ",currentPC[1],disassemble(currentInst[1]));
        valRa <= rf[currentInst[1].Normal.ra];
        valRb <= rf[currentInst[1].Normal.rb];
      end
    else if(stageValid(1))
      begin
      let read <- im.portA.response.get();
      let i = word2instruction(read);
      let ib = pack(i);
      currentInst[1] <= i;
      $display("| pm[%2d] : ",currentPC[1],disassemble(i));
      // read registers
      //valRa <= rf[ib[23:17]]; // hacky - fetch registers regardless of whether an immediate is going to be used
      //valRb <= rf[ib[16:10]];
      valRa <= rf[i.Normal.ra];
      valRb <= rf[i.Normal.rb];
      end
    else
      begin
      $display("| flush");
      if(!first)
        let read <- im.portA.response.get(); //Consume 
      
      end

  if(stageValid(2))
  begin
    WordT alu_result;
    case (currentInst[2]) matches
    tagged Normal {func:.func, shift:.shift, op:.op, skip:.skip}:
      begin
      WordT alu_calc;
      //$display(skip==SkipZero," ",(skip==SkipZero) && (alu==0)," ",((skip==SkipNeg) && (alu<0)) || ((skip==SkipZero) && (alu==0)) ||
      //((skip==SkipInRdy) && inQ.notEmpty));
      // initiate loads
      if(op==OpLoadDM)
        dm.portA.request.put(BRAMRequest{write: False, responseOnWrite: False, address: unpack(truncate(pack(valRb))), datain: 0});
      // first do the ALU operation
      case(func)
        FaADDb: alu_calc = valRa+valRb;
        FaSUBb: alu_calc = valRa-valRb;
        FINCb:  alu_calc = valRb+1;
        FDECb:  alu_calc = valRb-1;
        FaANDb: alu_calc = valRa & valRb;
        FaORb:  alu_calc = valRa | valRb;
        FaXORb: alu_calc = valRa ^ valRb;
        default: begin alu_calc = 0;  end//$finish;
      endcase
      // then do the shifter (rotates)
      Bit#(32) alub = pack(alu_calc); // ALU result in bits
      case(shift)
        ShiftNone:  alu_result = unpack(alub);
        ShiftRCY1:  alu_result = unpack({alub[30:0],alub[31]});
        ShiftRCY8:  alu_result = unpack({alub[23:0],alub[31:24]});
        ShiftRCY16: alu_result = unpack({alub[15:0],alub[31:16]});
        default:    alu_result = unpack(alub);
        endcase
      end
    tagged Immediate {rw:.rw, imm:.imm}:
      begin
      alu_result = zeroExtend(unpack(pack(imm)));
      end
      default: alu_result = 0;
    endcase
    alu <= alu_result;
  end

  if(stageValid(3))
    begin

    case (currentInst[3]) matches
    tagged Normal {op:.op, rw:.rw, skip:.skip}:
      begin
      let doSkip_temp =  ((skip==SkipNeg) && (alu<0)) ||
                      ((skip==SkipZero) && (alu==0)) ||
                      ((skip==SkipInRdy) && inQ.notEmpty);
      doSkip <= doSkip_temp;
      doSkipN = doSkip_temp;
      pc <= doSkip_temp ? pc+2 : ((op==OpJump) ? unpack(truncate(pack(alu))) : pc+1);
      Maybe#(WordT) wbval=tagged Invalid;
      case (op)
        OpNormal, OpStoreDM, OpStoreIM, OpOut: wbval = tagged Valid alu;
        OpLoadDM: begin
         let read <- dm.portA.response.get();
         wbval = tagged Valid read;
        end
        OpIn:     begin
         wbval = tagged Valid inQ.first;
         inQ.deq;
        end
        OpJump:  begin
         jumping <= True; 
         jumpingN = True;
        end
      endcase
      if(isValid(wbval))
        begin
        rf[rw] <= fromMaybe(?,wbval);
        $display("%05t: r%1d <- %1d",$time,rw,fromMaybe(?,wbval));
        end
      else
        $display("%05t: alu = %1d",$time,alu);
      PCT addr = unpack(truncate(pack(valRb_WB2)));
      case (op)
        OpStoreDM: 
          begin
          dm.portB.request.put(BRAMRequest{write: True, responseOnWrite: False, address: addr, datain: valRa_WB2});
          end
        OpStoreIM:
          begin
          im.portB.request.put(BRAMRequest{write: True, responseOnWrite: False, address: addr, datain: valRa_WB2});
          jumping <= True;
          jumpingN = True;
          $display("storeIM ",valRa_WB," ",mWriteData);
          end
        OpOut:     outQ.enq(alu);
        OpReserved: $finish;
      endcase
      end
      tagged Immediate {rw:.rw, imm:.imm}:
        begin
        pc <= pc+1;
        rf[rw] <= alu;
        $display("%05t: r%1d = imm = %1d",$time,rw,alu);
        end
    endcase
    //$display("%05t: doSkip = %s, alu = %1d",$time,doSkip_temp ? "True" : "False",alu);
    end
    
    let cHazardN = doSkipN || jumpingN;
    let stallNowN = False;

    if(cHazardN||flush)
      begin
      toWriteValid <= False;
      toWriteValid2 <= False;
      stall <= False;
      stall2 <= False;
      end
    else
      begin
      case (currentInst[1]) matches
        tagged Normal {op: .op, rw: .rw, ra: .ra, rb: .rb} :
          begin
          let shortStall = ((toWrite2==ra||toWrite2==rb)&&toWriteValid2); //1 cycle data hazard
          let longStall = ((toWrite==ra||toWrite==rb)&&toWriteValid); //2 cycle data hazard
          stallNowN = (shortStall||longStall)&&!flush;
          //stallNow = (((toWrite==ra||toWrite==rb)&&toWriteValid)||((toWrite2==ra||toWrite2==rb)&&toWriteValid2))&&!flush;
          if(stallNowN)
            begin
              if(longStall)
              begin
                stall <= True;
                toWriteValid <= False;
                toWriteValid2 <= False;
              end
              else
              begin
                stall2 <= True;
                toWriteValid <= False;
                toWriteValid2 <= toWriteValid;
                toWrite2 <= toWrite;
              end
            end
          else if(stall)
            begin
            stall <= False;
            stall2 <= True;
            end
          else if(op==OpReserved)
            begin
            toWriteValid<=False;
            toWrite2 <= toWrite;
            toWriteValid2 <= toWriteValid;
            stall <= False;
            stall2 <= False;
            end
          else
            begin
            toWrite <= rw;
            toWriteValid <= True;
            toWrite2 <= toWrite;
            toWriteValid2 <= toWriteValid;
            stall <= False;
            stall2 <= False;
            end
          end
        tagged Immediate {rw: .rw} ://Not sure whether .a is safe, but it won't accept not binding
          begin
          stall <= False;
          toWriteValid <= True;
          toWrite <= rw;
          toWrite2 <= toWrite;
          toWriteValid2 <= toWriteValid;
          end
      endcase
      end  

      let pcNext_calc;

       if(cHazardN&&stageValid(3)&&!flush&&!flush2)
      begin
      flush <= True;
      valid[0] <= True;
      valid[1] <= False;
      valid[2] <= False;
      valid[3] <= False;
      pcNext_calc = pc;
      end
    else if (flush)
      begin
      flush <= False;
      flush2 <= True;
      valid[0] <= True;
      valid[1] <= valid[0];
      valid[2] <= valid[1];
      valid[3] <= valid[2];
      pcNext_calc = pc + 1;
      end
    else if (flush2)
      begin
      flush2 <= False;
      flush3 <= True;
      valid[0] <= True;
      valid[1] <= valid[0];
      valid[2] <= valid[1];
      valid[3] <= valid[2];
      pcNext_calc = pc + 2;
      end
    else if(stallNowN)
      begin
      valid[0] <= False;
      valid[1] <= False;
      valid[2] <= False;
      valid[3] <= True;
      pcNext_calc = pc + 3;
      end
    else if(stall)
      begin
      valid[0] <= False;
      valid[1] <= False;
      valid[2] <= False;
      valid[3] <= False;
      pcNext_calc = pc + 2;
      end
    else if(stall2)
      begin
      valid[0] <= True;
      valid[1] <= True;
      valid[2] <= True;
      valid[3] <= False;
      pcNext_calc = pc + 2;
      end
    else
      begin
      if(flush3) flush3 <= False;
      pcNext_calc = pc + 3;
      valid[0] <= True;
      valid[1] <= valid[0];
      valid[2] <= valid[1];
      valid[3] <= valid[2];
      end

      pcNext <= pcNext_calc;
      currentPC <= stageStep(currentPC,pcNext_calc);
      //currentInst <= stageStep(currentInst,unpack(0));
    let temp = currentInst;
    temp[2] = currentInst[1];
    temp[3] = currentInst[2];
    //currentInst[2] <= currentInst[1];
    //currentInst[3] <= currentInst[2];
    currentInst <= temp;

    //$display(clockCount," ",pc," ",pcNext_calc," | ",cHazard," ",flush," ",flush2," | ",jumping," ",stallNow," ",stall," ",stall2," | ",toWrite," ",toWriteValid,", ",toWrite2, " ",toWriteValid2, " | ",currentInst[1].Normal.ra," ",currentInst[1].Normal.rb," |", currentPC[0], currentPC[1], currentPC[2], currentPC[3]);
    $display(clockCount," ",pc," ",pcNext_calc," | ",cHazardN," ",flush," ",flush2," | ",jumpingN," ",stallNowN," ",stall," ",stall2," | ",toWrite," ",toWriteValid,", ",toWrite2, " ",toWriteValid2, " | ",currentInst[1].Normal.ra," ",currentInst[1].Normal.rb," |", currentPC[0], currentPC[1], currentPC[2], currentPC[3]);
    $display("new",clockCount, " ", "pc", " ","pcNext_calc", " | " , cHazardN," ", "flush"," ", "flush2", " | ", jumpingN, " ", "stallNow", " ", "stall", " ", "stall2", " | ");
    clockCount <= clockCount+1;
    
    phase <= 1;
  endrule

  rule post(phase==1);


    let cHazard = doSkip || jumping;
    let stallNow = False;
    
      
    let pcNext_calc; //Shouldn't need to init
     


    
    
    

    valRa_WB <= valRa;
    valRb_WB <= valRb;
    valRa_WB2 <= valRa_WB;
    valRb_WB2 <= valRb_WB;

    //clockCount <= clockCount+1;

    jumping <= False;
    doSkip <= False;
    if(first)
      first <= False;
    phase <= 0;
    $display(clockCount," ",pc," ","pcNext_calc"," | ",cHazard," ",flush," ",flush2," | ",jumping," ",stallNow," ",stall," ",stall2," | ",toWrite," ",toWriteValid,", ",toWrite2, " ",toWriteValid2, " | ",currentInst[1].Normal.ra," ",currentInst[1].Normal.rb," |", currentPC[0], currentPC[1], currentPC[2], currentPC[3]);
  endrule




  interface in = toPut(inQ);
  interface out = toGet(outQ);

endmodule


endpackage
