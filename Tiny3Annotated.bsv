package Tiny3Annotated;

import FIFO::*;
import FIFOF::*;
import GetPut::*;
import TinyTypes::*;
import TinyAsm::*;
import BRAM::*;
import Vector::*;

interface TinyCompIfc;
   interface Put#(WordT) in;
   interface Get#(WordT) out;
endinterface 


module mkTinyComp(InstructionROM_T irom, TinyCompIfc ifc);
   //Registers
   Reg#(PCT) pc <- mkReg(0); //UInt#(10) Program Counter
   Reg#(WordT) alu <- mkReg(0); //Int#(32) ALU out of Words
   Reg#(UInt#(3)) phase <- mkReg(7); //Internal phase counter
   Reg#(Bool) doSkip <- mkRegU; //Un-init Bool for skip
   FIFOF#(WordT) inQ <- mkFIFOF; //Input FIFO Int#(32) of Words
   FIFO#(WordT) outQ <- mkFIFO; //Output FIFO Int#(32) of Words
   Vector#(TExp#(SizeOf#(PCT)),Reg#(WordT)) rf <- replicateM(mkRegU); //Vector of size 2^ProgramCounterSize of Words, 1024(?) registers
   Reg#(WordT) valRa <- mkRegU; //Ra instruction registers
   Reg#(WordT) valRb <- mkRegU; //Rb instruction registers
   Reg#(InstructionT) inst <- mkRegU; //Current instruction

   BRAM1Port#(PCT, WordT) im <- mkBRAM1Server(defaultValue); //Instruction memory
   BRAM1Port#(PCT, WordT) dm <- mkBRAM1Server(defaultValue); //Data memory
   Reg#(PCT) addr <- mkReg(0); //Address
   
   // initialise instruction memory (im) contents from ROM
   // Loops through all of the List# of instructions placing into ROM
   rule do_init(phase==7);
      WordT data = unpack(pack(irom[addr])); //Take the next instruction from the irom and convert to wordT.
      im.portA.request.put(BRAMRequest{write: True, responseOnWrite: False, //Write instruction to instruction memory.
					address: addr, datain: data});
      
      InstructionT inst = unpack(pack(data)); //Take WordT instruction and convert to InstructionT
      if(inst.Normal.op!=OpReserved) //if not reserved instruction, print instruction
	 $display("%05t: init pm[%1d] = ",$time,addr,disassemble(inst));
      let next_addr = addr+1; //Move to next instruction
      addr <= next_addr;
      if(next_addr==0) //If next addr is 0 (i.e rolled over) then move to next phase
	 phase <= 0;
   endrule
   
   //Fetches next instruction from memory
   rule fetch(phase==0);
      im.portA.request.put(BRAMRequest{write: False, responseOnWrite: False, address: pc, datain: 0}); //Request the instruction from the pc's index.
      $write("%05t: fetch",$time);
      phase <= 1;
   endrule

   //Fetches any registers needed from memory
   rule register_fetch(phase==1);
      let read <- im.portA.response.get(); //Read the returned instruction (next instruction)
      let i = word2instruction(read); //Unpack/repack as InstructionT shortcut
      let ib = pack(i); //Pack InstructionT
      inst <= i; //Set current instruction to fetched instruction
      $display(" pm[%2d] : ",pc,disassemble(i));
      // read registers
      valRa <= rf[ib[23:17]]; // hacky - fetch registers regardless of whether an immediate is going to be used
      valRb <= rf[ib[16:10]];
      phase <= 2;
   endrule
   
   rule execute(phase==2);
      WordT alu_result; //WordT for alu result
      case (inst) matches //Switch on the current instruction
	 tagged Normal {func:.func, shift:.shift, op:.op, skip:.skip}: //If tagged normal, place into normal structure and access func, shift, skip and op.
	    begin
	       WordT alu_calc;
	       doSkip <= ((skip==SkipNeg) && (alu<0)) ||
	                 ((skip==SkipZero) && (alu==0)) ||
 			 ((skip==SkipInRdy) && inQ.notEmpty); //If instruction says skip and condition is met, set doSkip true
	       // initiate loads
	       if(op==OpLoadDM) //If load data, place data load request
		  dm.portA.request.put(BRAMRequest{write: False, responseOnWrite: False, address: unpack(truncate(pack(valRb))), datain: 0}); //data load request for value in Rb
	       // first do the ALU operation
	       case(func) //Switch on function
		  FaADDb: alu_calc = valRa+valRb;
		  FaSUBb: alu_calc = valRa-valRb;
		  FINCb:  alu_calc = valRb+1;
		  FDECb:  alu_calc = valRb-1;
		  FaANDb: alu_calc = valRa & valRb;
		  FaORb:  alu_calc = valRa | valRb;
		  FaXORb: alu_calc = valRa ^ valRb;
		  default: begin alu_calc = 0; $finish; end //If function does not match then kill program(?)
	       endcase
	       // then do the shifter (rotates)
	       Bit#(32) alub = pack(alu_calc); // ALU result in bits
	       case(shift)
		  ShiftNone:  alu_result = unpack(alub);
		  ShiftRCY1:  alu_result = unpack({alub[30:0],alub[31]});
		  ShiftRCY8:  alu_result = unpack({alub[23:0],alub[31:24]});
		  ShiftRCY16: alu_result = unpack({alub[15:0],alub[31:16]});
		  default:    alu_result = unpack(alub);
	       endcase
	    end
	 tagged Immediate {rw:.rw, imm:.imm}: //If tagged immediate, place into Immediate structure and access rw and imm.
	    begin
	       doSkip <= False; //Never skip
	       alu_result = zeroExtend(unpack(pack(imm))); //Take the immediate value and prepend zeros to make a WordT
	    end
	 default: alu_result = 0; //If untagged, set to 0
      endcase
      alu <= alu_result; //Move result into alu
      phase <= 3;
   endrule
   

   rule write_back(phase==3);
      $display("%05t: doSkip = %s",$time,doSkip ? "True" : "False");
      case (inst) matches
	 tagged Normal {op:.op, rw:.rw}: //If instruction tagged normal, access op and rw
	    begin
	       pc <= doSkip ? pc+2 : ((op==OpJump) ? unpack(truncate(pack(alu))) : pc+1); //pc+1 unless skip then pc+2 or jump then pc=truncated alu.
	       Maybe#(WordT) wbval=tagged Invalid; //write-back value initially invalid
	       case (op)// Case on operation
		  OpNormal, OpStoreDM, OpStoreIM, OpOut: wbval = tagged Valid alu; //if normal, store DM/IM or out then write-back is alu and valid.
		  OpLoadDM: begin //If loadDM then get the response from the data memory and set valid.
			       let read <- dm.portA.response.get();
			       wbval = tagged Valid read;
			    end
		  OpIn:     begin //If input then get front of input queue and set valid.
			       wbval = tagged Valid inQ.first;
			       inQ.deq;
			    end
	       endcase
	       if(isValid(wbval)) //If valid
		  begin
		     rf[rw] <= fromMaybe(?,wbval); //If valid then set register at write location to value, else don't care
		     $display("%05t: r%1d <- %1d",$time,rw,fromMaybe(?,wbval));
		  end
	       else
		  $display("%05t: alu = %1d",$time,alu);
	       PCT addr = unpack(truncate(pack(valRb))); //Address is Rb truncated
	       case (op)
		  OpStoreDM: dm.portA.request.put(BRAMRequest{write: True, responseOnWrite: False, address: addr, datain: valRa}); //Write Ra to DM[addr]
		  OpStoreIM: im.portA.request.put(BRAMRequest{write: True, responseOnWrite: False, address: addr, datain: valRa}); //Write Ra to IM[addr]
		  OpOut:     outQ.enq(alu); //Enqueue alu onto output queue
		  OpReserved: $finish; //If reserved, exit
	       endcase
	    end
	 tagged Immediate {rw:.rw, imm:.imm}: //If tagged immediate, simple
	    begin
	       pc <= pc+1; //Increment pc
	       rf[rw] <= alu; //Write alu to register rw
	       $display("%05t: r%1d = imm = %1d",$time,rw,alu);
	    end
      endcase
      phase <= 0;
   endrule
   
   interface in = toPut(inQ);
   interface out = toGet(outQ);

endmodule


endpackage
