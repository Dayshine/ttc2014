package TinyTestBench;

import FIFO::*;
import GetPut::*;
import StmtFSM::*;
import TinyTypes::*;
import TinyAsm::*;
import Tiny3::*;
import BRAM::*;
import TestSuite::*;

module mkTestBenchA(Empty);

   /********** the program we wish to assemble **********/
   function List#(AsmLineT) my_program(function ImmT findaddr(String label)) =
      cons(asmi("",    1, 1), // put 0 in r0
      cons(asmi("",    1, 10), // put 10 in r1
      cons(asmi("",    2, findaddr("loop")), // loop address in r2
      cons(asm("loop", OpOut, FDECb, ShiftNone, SkipNever, 1, 0, 1), // r1-- and output
      cons(asm("",     OpJump, FaORb, ShiftNone, SkipZero, 3, 2, 0), // jump r2 if not zero
      cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
      tagged Nil))))));

   function List#(AsmLineT) timesTwo(function ImmT findaddr(String label)) =
      cons(asmi("",    0, 0),
      cons(asmi("",    1, 20),
      cons(asmi("",    2, 0),
      cons(asmi("",    3, findaddr("loop")),
      cons(asm("loop", OpNormal, FINCb, ShiftNone, SkipNever, 2, 0, 2),
      cons(asm("", OpNormal, FINCb, ShiftNone, SkipNever, 2, 0, 2),
      cons(asm("", OpNormal, FDECb, ShiftNone, SkipZero, 1, 0, 1),
      cons(asm("", OpNormal, FaORb, ShiftNone, SkipZero, 0, 0, 0),
      cons(asm("", OpJump, FaORb, ShiftNone, SkipNever, 4, 3, 0),
      cons(asm("", OpOut, FaORb, ShiftNone, SkipNever, 2, 2, 2),
      cons(asm("", OpNormal, FaORb, ShiftNone, SkipNever, 0, 2, 0),
      cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 1, 1, 1),
      tagged Nil))))))))))));

   function List#(AsmLineT) incTest(function ImmT findaddr(String label)) =
      cons(asmi("",  0, 0),
      cons(asmi("",  1, 1),
      cons(asm("", OpNormal, FINCb, ShiftNone, SkipNever, 0, 1, 1),
      cons(asm("", OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0),
      tagged Nil))));

   function List#(AsmLineT) jumpTest(function ImmT findaddr(String label)) =
      cons(asmi("start", 0 ,0),
      cons(asmi("", 1, findaddr("start")),
      cons(asm("", OpJump, FaORb, ShiftNone, SkipNever, 0, 1, 1),
      cons(asm("", OpNormal, FaORb, ShiftNone, SkipNever, 0, 0, 0),
      tagged Nil))));

   InstructionROM_T irom = assembler(testThreeb);
   
   TinyCompIfc tiny <- mkTinyComp(irom);

   rule handle_output;
      let out <- tiny.out.get();
      $display("%05t: output = %d\n",$time,out);
   endrule

endmodule



endpackage: TinyTestBench

