#!/bin/bash

#Width, Length
width=$(printf "%08X" $1 | grep -o .. | tac | tr -d '\n')
height=$(printf "%08X" $2 | grep -o .. | tac | tr -d '\n')
#Assume DIB header of 40, it's not huge
DIBsize="28000000"
dataSizeD=$(($1*$2*4))
dataSize=$(printf "%08X" $dataSizeD | grep -o .. | tac | tr -d '\n')
#Assume Header is 14 + DIB of 40
offset="36000000"

sizeD=$((54+$dataSizeD))
size=$(printf "%08X" $sizeD | grep -o .. | tac | tr -d '\n')
BMPHEADER="424d $size 0000 0000 $offset"
#One plane, but 32bit to simplify, no compression, 1 p/m both ways, 0 colours, 0 important
DIB="$DIBsize $width $height 0100 2000 00000000 $dataSize 10000000 10000000 00000000 00000000"
HEADER=$BMPHEADER$DIB

TESTBODY="0000ff00 ffffff00 ff000000 00ff0000"

DESTFILE=header.bmp
IMAGE="424d46000000 0000 0000 3600 0000 2800 0000 0200\
0000 0200 0000 0100 1800\
0000 0000 1000 0000 130b\
0000 130b 0000 0000 0000\
0000 0000 0000 ffff ffff\
0000 ff00 00ff 3399 0000"

IMAGE=$HEADER
area=$(($1*$2/4))
for((i=1;i<=$area;i++))
do
	IMAGE=$IMAGE$TESTBODY
done

echo -n $HEADER | xxd -r -p > $DESTFILE